export default async ({ store }) => {
  if (!store.state.auth.loggedIn) {
    return
  }
  const essential = [
    store.$db().model('users').fetch(),
    store.$db().model('services').fetch(),
    store.$db().model('parameters').fetch(),
  ]
  await Promise.all(essential)
}
