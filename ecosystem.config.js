module.exports = {
  apps: [
    {
      name: 'CTP-Nuxt-Cluster',
      exec_mode: 'cluster',
      instances: 'max', // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start',
      error_file: './logs/err.log',
      out_file: './logs/out.log',
      time: true,
    },
  ],
}
