import { users, messages, stats } from '@/apiClient'

export default ({ $axios }, inject) => {
  const api = {
    users: users($axios),
    messages: messages($axios),
    stats: stats($axios),
  }
  inject('api', api)
}
