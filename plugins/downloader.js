/**
 * @param axios
 * @return {function(string, string): Promise<void>}
 */
const downloadFactory = (axios) => (url, filename) =>
  axios.$get(url, { responseType: 'blob' }).then((file) => {
    const anchor = document.createElement('a')
    anchor.href = URL.createObjectURL(file)
    anchor.setAttribute('download', filename)
    anchor.click()
  })

export default ({ $axios }, inject) => {
  const downloader = {
    download: downloadFactory($axios),
  }
  inject('downloader', downloader)
}
