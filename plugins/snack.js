export default ({ store }, inject) => {
  const snack = {
    toggle: (payload) => store.dispatch('toggleFeedbackSnack', payload),
    toggleError: (message) =>
      store.dispatch('toggleFeedbackSnack', { message, type: 'error' }),
    toggleWarning: (message) =>
      store.dispatch('toggleFeedbackSnack', { message, type: 'warning' }),
    toggleSuccess: (message) =>
      store.dispatch('toggleFeedbackSnack', { message, type: 'success' }),
    toggleValidationSnack: () =>
      store.dispatch('toggleFeedbackSnack', {
        message: 'Please fix validation errors!',
        type: 'error',
      }),
  }
  inject('snack', snack)
}
