export default (axios) => ({
  getConnectorStatus() {
    return axios.$get('/api/connector-status')
  },
})
