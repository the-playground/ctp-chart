export default (axios) => ({
  /**
   * @param {string} content - Base64 string e.g. iVBORw0KGgoAAAANSUhEUgAAAoAAAAKAC
   * @param {string} filename
   * @param {'pdfs'|'xmls'} type
   * @return {Promise}
   */
  uploadFile(content, filename, type) {
    const payload = { filename, content, type }
    return axios.post('/api/users/files', payload)
  },

  /**
   * @param {string[]} filenames
   * @param {'pdfs'|'xmls'} type
   * @return {Promise}
   */
  submitFilenames(filenames, type) {
    const payload = { filenames, type }
    return axios.post('/api/users/filenames', payload)
  },
})
