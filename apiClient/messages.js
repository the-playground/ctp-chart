export default (axios) => ({
  /**
   * @return {Promise<{xmls: string[], pdfs: []}>}
   */
  getPredefinedFiles() {
    return axios.$get('/api/messages/predefined-files')
  },

  validateXml(id, xmlContent) {
    return axios.$get(`/api/messages/${id}/xml/validate`)
  },
})
