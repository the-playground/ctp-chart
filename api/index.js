import express from 'express'
// import libxmljs from 'libxmljs'
import passport from './passport'
import db from './db'
import routes from './routes'
import authRoutes from './routes/auth'

db.connectToDatabase()
const app = express()
app.use(passport.initialize())
app.use(passport.session())

app.use(express.json({ limit: '15mb' }))
app.use(express.urlencoded({ extended: true }))

app.use(authRoutes)
app.use(routes)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app,
}
