import mongoose from 'mongoose'
import dotenv from 'dotenv'
dotenv.config()

const connectToDatabase = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    console.log('MongoDB connected...')
  } catch (error) {
    console.log('connection error: ' + error)
  }
}

export default { connectToDatabase }
