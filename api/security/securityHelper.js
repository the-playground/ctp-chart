const adminGuard = (req, res, next) => {
  if (!req.user.isAdmin()) {
    return res.status(403).json({
      error: 'Not enough privileges',
    })
  }
  next()
}

export { adminGuard }
