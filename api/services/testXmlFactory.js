import filesystem from 'fs'
import path from 'path'
import { Jsonix } from 'jsonix'
import format from 'xml-formatter'
import assertionMappings from '../xml/mappings/assertionMappings'

const createAssertionXml = (assertion) => {
  const context = new Jsonix.Context([assertionMappings], {
    namespacePrefixes: {
      'http://www.w3.org/2001/XMLSchema-instance': 'xsi',
      'http://docs.oasis-open.org/ns/tag/taml-201002/': '',
    },
  })
  const marshaller = context.createMarshaller()

  const jsonixObject = {
    name: {
      localPart: 'testAssertion',
    },
    value: { ...assertion.toJSON() },
  }
  const marshalledXML = marshaller.marshalString(jsonixObject)
  const assertionPath = path.join(
    __dirname,
    '/../xml/assertions',
    `${assertion.testId}.xml`
  )
  filesystem.writeFile(assertionPath, format(marshalledXML), () => {})
}

export default { createAssertionXml }
