import { readFile } from 'fs/promises'
import path from 'path'
import cookie from 'cookie'
import MessageFile from '../models/lean/messageFile'
import RawMessage from '../models/lean/rawMessage'
import NewMessage from '../models/lean/newMessage'
import messageFileTypes from '../../constants/messageFileTypes'
import connectorClient from '../connectorClient'
const {
  PARTY_DEFAULT_FROM,
  PARTY_ROLE_TO,
  PARTY_ROLE_FROM,
  PARTY_TYPE_TO,
  PARTY_TYPE_FROM,
} = process.env

const getMessage = async (req, res) => {
  try {
    const { id } = req.params
    // TODO: Check if user is authorized to get this message
    const message = new RawMessage(await connectorClient.getMessageById(id))
    return res.json({ message })
  } catch (error) {
    return res.status(500).json({ error: 'Error fetching message' })
  }
}

const getMessages = async (req, res) => {
  try {
    const messages = await connectorClient.getAllMessages()
    // TODO: Filter messages only belonging to user (by gateway)
    return res.json({
      messages: messages.map((message) => new RawMessage(message)),
    })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Error fetching messages' })
  }
}

const getUpdatedMessages = async (req, res) => {
  try {
    const messages = await connectorClient.getAllMessages()
    // TODO: Filter messages only belonging to user (by gateway)
    const cookies = cookie.parse(req.headers.cookie || '')
    const checkDate = cookies.check_message_date

    const newMessages = messages
      .map((message) => new RawMessage(message))
      .filter(
        (message) =>
          message.created > checkDate || message.lastUpdated > checkDate
      )

    return res.json({
      messages: newMessages,
    })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Error fetching updated messages' })
  }
}

const getPredefinedFiles = async (req, res) => {
  const getUserDirectoryPath = (username, type) =>
    path.join(__dirname, `../files/users/${username}/${type}`)

  try {
    const defaultXmlPath = path.join(__dirname, '../files/xml/default.xml')
    const defaultPdfPath = path.join(__dirname, '../files/pdf/default.pdf')
    const xmls = {
      default: (await readFile(defaultXmlPath)).toString('base64'),
    }
    const pdfs = {
      default: (await readFile(defaultPdfPath)).toString('base64'),
    }

    const userXmls = req.user.xmls || []
    const userPdfs = req.user.pdfs || []

    const xmlDirectoryPath = getUserDirectoryPath(req.user.username, 'xmls')
    for (const userXml of userXmls) {
      const xmlPath = xmlDirectoryPath + '/' + userXml
      xmls[userXml] = (await readFile(xmlPath)).toString('base64')
    }

    const pdfDirectoryPath = getUserDirectoryPath(req.user.username, 'pdfs')
    for (const userPdf of userPdfs) {
      const pdfPath = pdfDirectoryPath + '/' + userPdf
      pdfs[userPdf] = (await readFile(pdfPath)).toString('base64')
    }

    return res.json({ xmls, pdfs })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Error fetching predefined files' })
  }
}

/**
 * Creates bare message as sent by step 1 (message creation)
 */
const createMessage = async (req, res) => {
  try {
    const submittedMessage = req.body.message
    const message = new NewMessage({
      conversationId: submittedMessage.conversationId,
      originalSender: submittedMessage.originalSender,
      finalRecipient: submittedMessage.finalRecipient,
      service: submittedMessage.service,
      action: submittedMessage.action,
      fromPartyId: PARTY_DEFAULT_FROM,
      fromPartyRole: PARTY_ROLE_FROM,
      fromPartyType: PARTY_TYPE_FROM,
      toPartyId: req.user.gateway,
      toPartyRole: PARTY_ROLE_TO,
      toPartyType: PARTY_TYPE_TO,
    })

    const connectorMessage = await connectorClient.saveMessage(message)
    return res.status(201).json({ message: new RawMessage(connectorMessage) })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Message submission failed' })
  }
}

/**
 * This api action accepts fileName, fileType and fileContent
 * payload properties and uses connectorClient to upload message
 * a single file
 */
const uploadMessageFile = async (req, res) => {
  try {
    if (!messageFileTypes.includes(req.body.fileContent)) {
      return res.status(400).json({ error: 'Invalid message file type' })
    }

    let storageInfo
    try {
      // TODO: Check if user is authorized to get this message
      const message = await connectorClient.getMessageById(req.params.id)
      storageInfo = message.storageInfo
    } catch (error) {
      console.log(error)
      return res
        .status(400)
        .json({ error: 'Could not fetch message before message file upload' })
    }

    const payload = {
      fileName: req.body.fileName,
      fileType: req.body.fileType,
      fileContent: req.body.fileContent,
      storageLocation: storageInfo,
    }
    await connectorClient.uploadMessageFile(payload)
    return res.status(200).json(payload)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Message file submission failed' })
  }
}

const submitMessage = async (req, res) => {
  try {
    // TODO: Check if user is authorized to get this message
    const { storageInfo } = await connectorClient.getMessageById(req.params.id)
    await connectorClient.submitStoredClientMessage(storageInfo)
    return res.status(204).json()
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Message file submission failed' })
  }
}

const getMessageFiles = async (req, res) => {
  try {
    // TODO: Check if user is authorized to get this message
    const message = await connectorClient.getMessageById(req.params.id)

    /**
     * For some reason connector returns files as an object { files: [] }
     * @type {Object[]}
     */
    const rawFiles = message.files.files

    const validTypes = Object.values(messageFileTypes)
    /** @type {MessageFile[]} */
    const messageFiles = rawFiles
      .map((file) => new MessageFile(file))
      .filter((messageFile) => validTypes.includes(messageFile.fileType))

    const files = {}
    for (const file of messageFiles) {
      const filePath = file.storageLocation + '/' + file.fileName
      files[file.fileName] = (await readFile(filePath)).toString('base64')
    }
    return res.json({ files })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Getting message files failed' })
  }
}

// TODO: Implement XML validation
const validateXml = async (req, res) => {
  try {
    // TODO: Check if user is authorized to get this message
    const message = await connectorClient.getMessageById(req.params.id)
    console.log(message)
    // const epoService = await Service.findOne({ name: 'EPO' }).exec()
    // const formAAction = epoService.actions.find(
    //   (action) => action.name === 'Form_A'
    // )
    // const actionDirectory = path.join(__dirname, 'files', 'actions')
    // const schemaPath = path.join(
    //   actionDirectory,
    //   'xsd',
    //   formAAction.defaultXsdPath
    // )
    // const xmlPath = path.join(
    //   actionDirectory,
    //   'xml',
    //   formAAction.defaultXmlPath
    // )
    // const sitemap = filesystem.readFileSync(xmlPath)
    // const schema = filesystem.readFileSync(schemaPath)
    //
    // const sitemapDoc = libxmljs.parseXml(sitemap)
    // const schemaDoc = libxmljs.parseXml(schema, {
    //   baseUrl: path.dirname(schemaPath) + '/',
    // })
    //
    // try {
    //   const isValid = sitemapDoc.validate(schemaDoc)
    //   return res.json({ flag: isValid })
    // } catch (error) {
    //   console.log(error)
    //   return res.status(500).json({ errors: sitemapDoc.validationErrors })
    // }
  } catch (error) {
    console.log(error)
    return res
      .status(500)
      .json({ error: `Validating XML failed for message id: ${req.params.id}` })
  }
}

export default {
  createMessage,
  getMessages,
  uploadMessageFile,
  submitMessage,
  getMessage,
  getPredefinedFiles,
  getUpdatedMessages,
  getMessageFiles,
  validateXml,
}
