import mongoose from 'mongoose'
import TestAssertion from '../models/TestAssertion'
import TestReport from '../models/TestReport'

const getTestAssertions = async (req, res, next) => {
  try {
    const assertions = (await TestAssertion.find().exec()).map((assertion) =>
      assertion.toJSON()
    )
    return res.json({ assertions })
  } catch (error) {
    return res.status(500).json({ error: 'Error getting test assertions' })
  }
}

const createTestAssertion = async (req, res, next) => {
  const assertion = new TestAssertion(req.body.assertion)
  await assertion.save()
  return res.status(201).json({ assertion: assertion.toJSON() })
}

const deleteTestAssertion = async (req, res, next) => {
  await TestAssertion.findByIdAndDelete(req.params.id).exec()
  return res.status(204).json()
}

const updateTestAssertion = async (req, res, next) => {
  const assertion = await TestAssertion.findById(req.params.id).exec()
  for (const property in req.body.assertion) {
    assertion[property] = req.body.assertion[property]
  }
  await assertion.save()
  return res.json({ assertion: assertion.toJSON() })
}

const assignTestAssertion = async (req, res, next) => {
  const assertion = await TestAssertion.findById(req.params.id).exec()
  const userId = req.params.userId
  if (!assertion.users.includes(userId)) {
    assertion.users.push(userId)
    await assertion.save()
  }
  return res.json({ assertion: assertion.toJSON() })
}

const unassignTestAssertion = async (req, res, next) => {
  const assertion = await TestAssertion.findById(req.params.id).exec()
  const userId = req.params.userId
  if (assertion.users.includes(userId)) {
    assertion.users = assertion.users.filter((id) => id.toString() !== userId)
    await assertion.save()
  }
  return res.json({ assertion: assertion.toJSON() })
}

const getTestAssertionsByUser = async (req, res, next) => {
  try {
    const assertions = (
      await TestAssertion.find({
        users: mongoose.Types.ObjectId(req.params.userId),
      })
        .populate({ path: 'reports' })
        .exec()
    ).map((assertion) => assertion.toJSON())
    return res.json({ assertions })
  } catch (error) {
    return res.status(500).json({ error: 'Error getting test assertions' })
  }
}

const createTestReport = async (req, res, next) => {
  const report = new TestReport({
    ...req.body.report,
    assertion: new mongoose.Types.ObjectId(req.params.assertionId),
    dateAuthored: new Date(),
    user: new mongoose.Types.ObjectId(req.user.id),
  })
  report.generateReportId()
  await report.save()
  return res.status(201).json({ report: report.toJSON() })
}

const updateTestReport = async (req, res, next) => {
  const report = await TestReport.findById(req.params.id).exec()
  for (const property in req.body.report) {
    report[property] = req.body.report[property]
  }
  await report.save()
  return res.json({ report: report.toJSON() })
}

const deleteTestReport = async (req, res, next) => {
  await TestReport.findByIdAndDelete(req.params.id).exec()
  return res.status(204).json()
}

export default {
  getTestAssertions,
  createTestAssertion,
  deleteTestAssertion,
  updateTestAssertion,
  assignTestAssertion,
  unassignTestAssertion,
  getTestAssertionsByUser,
  createTestReport,
  updateTestReport,
  deleteTestReport,
}
