import ConnectorStatus from '../models/ConnectorStatus'

const getConnectorStatus = async (req, res) => {
  try {
    const statuses = await ConnectorStatus.find()
    const sorted = statuses.sort((a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    })
    res.status(200).json(sorted)
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

const createConnectorStatus = async (req, res, next) => {
  const newStatus = new ConnectorStatus({ status: true })
  console.log(req.body)
  console.log(req.status)
  try {
    const status = await newStatus.save()
    if (!status) throw new Error("didn't saved")
    return res.status(200).json(status)
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

export default { getConnectorStatus, createConnectorStatus }
