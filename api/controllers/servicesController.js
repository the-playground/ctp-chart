import filesystem from 'fs'
import path from 'path'
import Service from '../models/Service'

const getServices = async (req, res, next) => {
  try {
    const services = (await Service.find().exec()).map((service) =>
      service.toJSON()
    )
    return res.json({ services })
  } catch (error) {
    return res.status(500).json({ error: 'Error getting services' })
  }
}

const createService = async (req, res, next) => {
  const service = new Service(req.body.service)
  await service.save()
  return res.status(201).json({ service: service.toJSON() })
}

const updateService = async (req, res, next) => {
  const service = await Service.findById(req.params.id).exec()
  for (const property in req.body.service) {
    service[property] = req.body.service[property]
  }
  await service.save()
  return res.json({ service: service.toJSON() })
}

const deleteService = async (req, res, next) => {
  await Service.findByIdAndDelete(req.params.id).exec()
  return res.status(204).json()
}

const addAction = async (req, res, next) => {
  const service = await Service.findById(req.params.id).exec()
  service.actions = [...service.actions, req.body.action]
  await service.save()
  return res.json({ service: service.toJSON() })
}

const editAction = async (req, res, next) => {
  const service = await Service.findById(req.params.id).exec()
  const action = service.actions.id(req.params.actionId)
  action.overwrite(req.body.action)
  await service.save()
  return res.json({ service: service.toJSON() })
}

const deleteAction = async (req, res, next) => {
  const service = await Service.findById(req.params.id).exec()
  service.actions.id(req.params.actionId).remove()
  await service.save()
  return res.json({ service: service.toJSON() })
}

const getActionXml = async (req, res, next) => {
  const service = await Service.findById(req.params.id).exec()
  const defaultXmlPath = service.actions.id(req.params.actionId).defaultXmlPath
  const actionDirectory = path.join(process.cwd(), 'files', 'actions')
  const xmlPath = path.join(actionDirectory, 'xml', defaultXmlPath)
  return res.json({ xml: filesystem.readFileSync(xmlPath) })
}

export default {
  getServices,
  createService,
  updateService,
  deleteService,
  addAction,
  editAction,
  deleteAction,
  getActionXml,
}
