import Parameter from '../models/Parameter'

const getParameters = async (req, res, next) => {
  try {
    const parameters = (await Parameter.find().exec()).map((parameter) =>
      parameter.toJSON()
    )
    return res.json({ parameters })
  } catch (error) {
    return res.status(500).json({ error: 'Error getting parameters' })
  }
}

const updateParameter = async (req, res, next) => {
  const parameter = await Parameter.findById(req.params.id).exec()
  parameter.overwrite(req.body.parameter)
  await parameter.save()
  return res.json({ parameter: parameter.toJSON() })
}

const createParameter = async (req, res, next) => {
  const parameter = new Parameter(req.body.parameter)
  await parameter.save()
  return res.status(201).json({ parameter: parameter.toJSON() })
}

const deleteParameter = async (req, res, next) => {
  await Parameter.findByIdAndDelete(req.params.id).exec()
  return res.status(204).json()
}

export default {
  getParameters,
  updateParameter,
  createParameter,
  deleteParameter,
}
