import { existsSync, mkdirSync } from 'fs'
import { writeFile } from 'fs/promises'
import path from 'path'
import User from '../models/User'

/**
 * @param {string} username
 * @param {"pdfs"|"xmls"} type
 * @return {string}
 */
const getUserDirectoryPath = (username, type) =>
  path.join(__dirname, `../files/users/${username}/${type}`)

const getUsers = async (req, res, next) => {
  try {
    const users = (await User.find().exec()).map((user) => user.toJSON())
    return res.json({ users })
  } catch (error) {
    return res.status(500).json({ error: 'Error getting users' })
  }
}

const updateUser = async (req, res, next) => {
  const user = await User.findById(req.params.id).exec()
  for (const property in req.body.user) {
    user[property] = req.body.user[property]
  }
  await user.save()
  return res.json({ user: user.toJSON() })
}

const updateUserPassword = async (req, res, next) => {
  const user = await User.findById(req.params.id).exec()
  // const {
  //   passwords: { currentPassword, newPassword },
  // } = req.body

  // const isValidPassword = await validateCurrentPassword(user, currentPassword)
  // if (!isValidPassword) {
  //   return res.status(403).json({ message: 'Invalid current password' })
  // }

  const newPassword = req.body.newPassword
  if (newPassword) {
    await user.setPassword(newPassword)
    await user.save()
    return res.status(204).json()
  }

  return res.status(400).json({ message: 'No password provided' })
}

const updateUserRoles = async (req, res, next) => {
  const id = req.params.id

  const user = await User.findById(id).exec()
  const { roles } = req.body
  user.roles = roles
  await user.save()
  return res.json({ user: user.toJSON() })
}

const deleteUser = async (req, res, next) => {
  await User.findByIdAndDelete(req.params.id).exec()
  return res.status(204).json()
}

const createUser = async (req, res, next) => {
  const user = new User({ email: req.body.email })
  await User.register(user, req.body.password)

  return res.status(201).json({ user: user.toJSON() })
}

// const validateCurrentPassword = async (user, password) =>
//   user.isAdmin() || (await user.authenticate(password))

const uploadFile = async (req, res) => {
  const { content, filename, type } = req.body

  if (!['pdfs', 'xmls'].includes(type)) {
    return res.status(400).json({ error: 'Invalid file type for user upload' })
  }

  try {
    const userDirectory = `../files/users/${req.user.username}`

    if (!existsSync(path.join(__dirname, userDirectory))) {
      mkdirSync(path.join(__dirname, userDirectory))
      mkdirSync(getUserDirectoryPath(req.user.username, 'xmls'))
      mkdirSync(getUserDirectoryPath(req.user.username, 'pdfs'))
    }

    const targetDirectory = getUserDirectoryPath(req.user.username, type)
    const filePath = `${targetDirectory}/${filename}`
    await writeFile(filePath, content, 'base64')
    return res.sendStatus(204)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'User xml upload failed' })
  }
}

const submitFilenames = async (req, res) => {
  const { filenames, type } = req.body
  if (!['pdfs', 'xmls'].includes(type)) {
    return res.status(400).json({ error: 'Invalid file type for user upload' })
  }

  try {
    req.user[type] = filenames
    await req.user.save()
    return res.sendStatus(204)
  } catch (error) {
    console.log(error)
    return res
      .status(500)
      .json({ error: 'User xml filenames submission failed' })
  }
}

const getFile = (req, res) => {
  const { filename, type } = req.params
  if (!['pdfs', 'xmls'].includes(type)) {
    return res.status(400).json({ error: 'Invalid file type for user upload' })
  }
  try {
    return res.sendFile(filename, {
      root: getUserDirectoryPath(req.user.username, type),
    })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ error: 'Getting user xml failed' })
  }
}

export default {
  getUsers,
  updateUser,
  updateUserPassword,
  updateUserRoles,
  deleteUser,
  createUser,
  uploadFile,
  submitFilenames,
  getFile,
}
