import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
dotenv.config()

const login = (req, res) => {
  try {
    const token = jwt.sign(req.user.toJSON() || {}, process.env.JWT_SECRET)
    return res.json({ token })
  } catch (e) {
    console.log(e)
    return res.status(500).json({ error: 'Login failure' })
  }
}

const fetchUser = (req, res) => {
  return res.json({ user: req.user || {} })
}

const logout = (req, res) => {
  req.logout()
  return res.sendStatus(204)
}

export default { login, fetchUser, logout }
