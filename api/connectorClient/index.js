import axios from 'axios'
const { REST_SERVICE_URL } = process.env

/**
 * Fetches message by id
 * @param id
 * @return {Promise<object>} - message object
 */
const getMessageById = async (id) => {
  const response = await axios.get(
    `${REST_SERVICE_URL}/getMessageById?id=${id}`
  )
  return response.data
}

/**
 * Given file name, type, content and storage location path
 * it uploads a message file by sending a request to Connector API client
 * As message file can be considered any related file that should be
 * included in the message (xml, pdf, other files, etc).
 *
 * @param {string} fileName
 * @param {string} fileType
 * @param {string} storageLocation
 * @param {string} fileContent
 * @return {Promise<void>}
 */
const uploadMessageFile = async ({
  fileName,
  fileType,
  fileContent,
  storageLocation,
}) => {
  const payload = {
    fileName,
    fileType,
    fileContent,
    storageLocation,
  }
  return await axios.post(`${REST_SERVICE_URL}/uploadMessageFile`, payload)
}

/**
 * @param {NewMessage} message
 * @return {Promise<Object>} - message object
 */
const saveMessage = async (message) => {
  const response = await axios.post(`${REST_SERVICE_URL}/saveMessage`, message)
  return response.data
}

/**
 * @param {string} storageInfo
 * @return {Promise<void>}
 */
const submitStoredClientMessage = (storageInfo) => {
  return axios.post(`${REST_SERVICE_URL}/submitStoredClientMessage`, {
    storageInfo,
  })
}

/**
 * @return {Promise<Object[]>} - array of message objects
 */
const getAllMessages = async () => {
  const response = await axios.get(`${REST_SERVICE_URL}/getAllMessages`)
  return response.data.messages
}

export default {
  getMessageById,
  uploadMessageFile,
  saveMessage,
  submitStoredClientMessage,
  getAllMessages,
}
