import mongoose from 'mongoose'
const { Schema } = mongoose

const ActionSchema = new Schema({
  name: { type: String, required: true },
  isOutgoing: { type: Boolean, default: false },
  isReply: { type: Boolean, default: false },
  // defaultXmlPath: { type: String, required: true },
  // defaultPdfPath: { type: String, required: true },
  // defaultXsdPath: { type: String, required: true },
})

const Service = new Schema({
  name: { type: String, required: true },
  shortcode: { type: String, required: true },
  test: { type: Boolean, default: false },
  caseOrder: { type: Number },
  actions: [ActionSchema],
})

Service.options.toJSON = {
  transform(doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  },
}

export default mongoose.model('services', Service)
