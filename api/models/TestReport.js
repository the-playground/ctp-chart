import mongoose from 'mongoose'
const { Schema } = mongoose

const Actor = new Schema({
  name: { type: String, required: true },
  uri: { type: String, required: false },
  contact: { type: String, required: true },
  role: { type: String, required: true },
})
const TestReport = new Schema({
  actors: [Actor],
  user: { type: Schema.ObjectId, ref: 'assertions' },
  performerName: { type: String, required: true },
  performerEmail: { type: String, required: true },
  assertion: { type: Schema.ObjectId, ref: 'assertions' },
  datePerformed: { type: Date, required: true },
  dateAuthored: { type: Date, required: true },
  result: {
    type: String,
    enum: ['SUCCESS', 'FAILURE', 'UNDEFINED'],
    required: true,
  },
  warnings: { type: String },
  issues: { type: String },
  reportId: { type: String, required: true },
  message: { type: String },
})

TestReport.methods.generateReportId = function () {
  const now = new Date().toISOString()
  const timestamp = now.substring(0, now.length - 6)
  const [date, time] = timestamp.split('T')
  const [year, month, day] = date.split('-')
  const [hours, minutes, seconds] = time.split(':')
  const dateTime = `${day}_${month}_${year}_${hours}_${minutes}_${seconds}`
  this.reportId = `TestReport_${dateTime}_${this.assertion}`
}

TestReport.options.toJSON = {
  transform(doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  },
}

export default mongoose.model('reports', TestReport)
