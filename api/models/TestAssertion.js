import mongoose from 'mongoose'
const { Schema } = mongoose

const AdditionalInfo = new Schema({
  name: { type: String, required: true },
  attribute: { type: String, required: true },
})

const TestAssertion = new Schema({
  testId: { type: String, required: true },
  normativeSource: { type: String, required: true },
  description: { type: String, required: true },
  prerequisite: { type: String, required: true },
  target: { type: String, required: true },
  prescription: { type: String, required: true },
  predicate: { type: String, required: true },
  tags: [AdditionalInfo],
  variables: [AdditionalInfo],
  users: [{ type: Schema.ObjectId, ref: 'users' }],
})

TestAssertion.virtual('reports', {
  ref: 'reports', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: 'assertion', // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: false,
})

TestAssertion.options.toJSON = {
  transform(doc, ret, options) {
    ret.id = ret._id
    ret.user_ids = ret.users.map((user) => user._id)
    delete ret.users
    delete ret._id
    delete ret.__v
    return ret
  },
  virtuals: true,
}

export default mongoose.model('assertions', TestAssertion)
