import mongoose from 'mongoose'
import passportLocalMongoose from 'passport-local-mongoose'
const { Schema } = mongoose

const User = new Schema({
  username: { type: String, required: true },
  email: { type: String, required: true },
  gateway: { type: String, required: true },
  finalRecipient: { type: String, required: true },
  originalSender: { type: String, required: true },
  contactPerson: { type: String, required: true },
  senderId: { type: String, required: true },
  senderAuth: { type: String, required: true },
  receiverId: { type: String, required: true },
  receiverAuth: { type: String, required: true },
  roles: [{ type: String, required: true }],
  gatewayVersion: String,
  connectorVersion: String,
  gatewayUrl: String,
  countryCode: String,
  xmls: [String],
  pdfs: [String],
})

User.methods.isAdmin = function () {
  return this.roles.includes('ROLE_ADMIN')
}

User.options.toJSON = {
  transform(doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.password
    delete ret.hash
    delete ret.salt
    delete ret.__v
    return ret
  },
}

User.plugin(passportLocalMongoose)
export default mongoose.model('users', User)
