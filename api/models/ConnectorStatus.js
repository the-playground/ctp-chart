import mongoose from 'mongoose'
const { Schema } = mongoose

const ConnectorStatusSchema = new Schema({
  status: { type: Boolean, required: true },
  createdAt: { type: Date, default: Date.now },
})

export default mongoose.model('connector-status', ConnectorStatusSchema)
