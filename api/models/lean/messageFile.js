export default class MessageFile {
  constructor({ fileName, fileType, fileContent = null, storageLocation }) {
    this.fileName = fileName
    this.fileType = fileType
    this.fileContent = fileContent
    this.storageLocation = storageLocation
  }
}
