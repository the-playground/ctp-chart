// Example filled object:
// {
//   "conversationId": "testui5",
//   "originalSender": "ejustice",
//   "finalRecipient": "ctp",
//   "service": "EPO",
//   "action": "Form_A",
//   "fromPartyId": "GR",
//   "fromPartyRole": "GW",
//   "fromPartyType": "urn=oasis=names=tc=ebcore=partyid-type=iso3166-1",
//   "toPartyId": "CTP",
//   "toPartyRole": "GW",
//   "toPartyType": "urn=oasis=names=tc=ebcore=partyid-type=unregistered",
// }
export default class NewMessage {
  constructor({
    conversationId = null,
    originalSender = null,
    finalRecipient = null,
    service = null,
    action = null,
    fromPartyId = null,
    fromPartyRole = null,
    fromPartyType = null,
    toPartyId = null,
    toPartyRole = null,
    toPartyType = null,
  } = {}) {
    this.conversationId = conversationId
    this.originalSender = originalSender
    this.finalRecipient = finalRecipient
    this.service = service
    this.action = action
    this.fromPartyId = fromPartyId
    this.fromPartyRole = fromPartyRole
    this.fromPartyType = fromPartyType
    this.toPartyId = toPartyId
    this.toPartyRole = toPartyRole
    this.toPartyType = toPartyType
  }
}
