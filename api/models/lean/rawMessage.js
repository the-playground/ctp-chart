export default class RawMessage {
  constructor({
    id,
    ebmsMessageId,
    backendMessageId,
    conversationId,
    originalSender,
    finalRecipient,
    fromPartyId,
    fromPartyType,
    fromPartyRole,
    toPartyId,
    toPartyRole,
    toPartyType,
    service,
    action,
    storageStatus,
    storageInfo,
    lastConfirmationReceived,
    messageStatus,
    created,
    evidences,
    files,
    instanceId,
    caseId,
  } = {}) {
    this.id = id
    this.ebmsMessageId = ebmsMessageId
    this.backendMessageId = backendMessageId
    this.conversationId = conversationId
    this.originalSender = originalSender
    this.finalRecipient = finalRecipient
    this.fromPartyId = fromPartyId
    this.fromPartyType = fromPartyType
    this.fromPartyRole = fromPartyRole
    this.toPartyId = toPartyId
    this.toPartyRole = toPartyRole
    this.toPartyType = toPartyType
    this.service = service
    this.action = action
    this.storageStatus = storageStatus
    this.storageInfo = storageInfo
    this.lastConfirmationReceived = lastConfirmationReceived
    /**
     * @type {'PREPARED', 'SENT', 'CONFIRMED'}
     */
    this.messageStatus = messageStatus
    this.created = created
    this.evidences = evidences
    // For some reason connector returns files as an object { files: [] }
    this.files = files.files
    this.instanceId = instanceId
    this.caseId = caseId
  }

  /**
   * @return {string|null} - date string
   */
  get lastUpdated() {
    const maxId = Math.max(...this.evidences.map((evidence) => evidence.id))
    if (!Number.isInteger(maxId)) {
      return null
    }
    return this.evidences.find((evidence) => evidence.id === maxId).received
  }
}
