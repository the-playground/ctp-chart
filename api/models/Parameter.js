import mongoose from 'mongoose'
const { Schema } = mongoose

const Parameter = new Schema({
  name: { type: String, required: true },
  value: { type: mongoose.Mixed, required: true },
})

Parameter.options.toJSON = {
  transform(doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  },
}

export default mongoose.model('parameters', Parameter)
