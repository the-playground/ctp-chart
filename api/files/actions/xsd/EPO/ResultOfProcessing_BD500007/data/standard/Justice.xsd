<?xml version="1.0" encoding="UTF-8"?>
 <!-- ====================================================================== -->
 <!-- ===== Reusable Aggregate Business Information Entity Schema Module ===== -->
 <!-- ===== Justice ===== -->
 <!--

  Courtesy of Judicial Information Agency of the Dutch Department of Security and Justice
 

 -->
 <xsd:schema xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2" xmlns:qdt="urn:un:unece:uncefact:data:standard:QualifiedDataType:11" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:udt="urn:un:unece:uncefact:data:standard:UnqualifiedDataType:11" xmlns:jus="http://data.ecodex.eu/jus/dictionary-0" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://data.ecodex.eu/jus/dictionary-0" version="1.8" xmlns="http://data.ecodex.eu/jus/dictionary-0">
  <!-- =================================================================== -->
  <!-- ===== Imports                                                       -->
  <!-- =================================================================== -->
  <!-- =================================================================== -->
  <!-- ===== Import of Unqualified DataType Schema Module                  -->
  <!-- =================================================================== -->
  <xsd:import namespace="urn:un:unece:uncefact:data:standard:UnqualifiedDataType:11" schemaLocation="UnqualifiedDataType.xsd"/>
  <!-- =================================================================== -->
  <!-- ===== Import of Qualified DataType Schema Module                    -->
  <!-- =================================================================== -->
  <xsd:import namespace="urn:un:unece:uncefact:data:standard:QualifiedDataType:11" schemaLocation="QualifiedDataType.xsd"/>
  <!-- =================================================================== -->
  <!-- ===== Element Declarations                                          -->
  <!-- =================================================================== -->
  <xsd:element name="Error" type="jus:ErrorType">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Definition>An error that was found in processing  the message</ccts:Definition>
        <ccts:Acronym>ABIE</ccts:Acronym>
        <ccts:DictionaryEntryName>Error. Details</ccts:DictionaryEntryName>
        <ccts:ObjectClassTerm>Error</ccts:ObjectClassTerm>
        <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  <xsd:element name="Statement" type="jus:StatementType">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Definition>A manifestation of will by a party to sue or oppose a legal claim&#13;</ccts:Definition>
        <ccts:Acronym>ABIE</ccts:Acronym>
        <ccts:DictionaryEntryName>Statement. Details</ccts:DictionaryEntryName>
        <ccts:ObjectClassTerm>Statement</ccts:ObjectClassTerm>
        <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  <!-- =================================================================== -->
  <!-- ===== Type Definitions                                              -->
  <!-- =================================================================== -->
  <!-- =================================================================== -->
  <!-- ===== Type Definition: ErrorType -->
  <!-- =================================================================== -->
  <xsd:complexType name="ErrorType">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Definition>An error that was found in processing  the message</ccts:Definition>
        <ccts:Acronym>ABIE</ccts:Acronym>
        <ccts:DictionaryEntryName>Error. Details</ccts:DictionaryEntryName>
        <ccts:ObjectClassTerm>Error</ccts:ObjectClassTerm>
        <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="Errorcode" type="udt:CodeType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Definition>A value in a codelist that explains the nature of the error</ccts:Definition>
            <ccts:Acronym>BBIE</ccts:Acronym>
            <ccts:Cardinality>0..1</ccts:Cardinality>
            <ccts:ObjectClassTerm>Error</ccts:ObjectClassTerm>
            <ccts:PropertyTerm>Errorcode</ccts:PropertyTerm>
            <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
            <ccts:PrimaryRepresentationTerm>Code</ccts:PrimaryRepresentationTerm>
            <ccts:DictionaryEntryName>Error. Errorcode. Code</ccts:DictionaryEntryName>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="LevelCode" type="udt:CodeType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Definition>An indication of the seriousness of the error denoted by a value from a codelist</ccts:Definition>
            <ccts:Acronym>BBIE</ccts:Acronym>
            <ccts:Cardinality>0..1</ccts:Cardinality>
            <ccts:ObjectClassTerm>Error</ccts:ObjectClassTerm>
            <ccts:PropertyTerm>Level</ccts:PropertyTerm>
            <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
            <ccts:PrimaryRepresentationTerm>Code</ccts:PrimaryRepresentationTerm>
            <ccts:DictionaryEntryName>Error. Level. Code</ccts:DictionaryEntryName>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="Description" type="udt:TextType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Definition>Explanation of the error in words </ccts:Definition>
            <ccts:Acronym>BBIE</ccts:Acronym>
            <ccts:Cardinality>0..1</ccts:Cardinality>
            <ccts:ObjectClassTerm>Error</ccts:ObjectClassTerm>
            <ccts:PropertyTerm>Description</ccts:PropertyTerm>
            <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
            <ccts:PrimaryRepresentationTerm>Text</ccts:PrimaryRepresentationTerm>
            <ccts:DictionaryEntryName>Error. Description. Text</ccts:DictionaryEntryName>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <!-- =================================================================== -->
  <!-- ===== Type Definition: StatementType -->
  <!-- =================================================================== -->
  <xsd:complexType name="StatementType">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Definition>A manifestation of will by a party to sue or oppose a legal claim&#13;</ccts:Definition>
        <ccts:Acronym>ABIE</ccts:Acronym>
        <ccts:DictionaryEntryName>Statement. Details</ccts:DictionaryEntryName>
        <ccts:ObjectClassTerm>Statement</ccts:ObjectClassTerm>
        <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <xsd:element name="AcceptanceIndicator" type="udt:IndicatorType" minOccurs="0">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Definition>An explicit or implicit manifestation of will by which one party agrees on what is proposed by the other party&#13;</ccts:Definition>
            <ccts:Acronym>BBIE</ccts:Acronym>
            <ccts:Cardinality>0..1</ccts:Cardinality>
            <ccts:ObjectClassTerm>Statement</ccts:ObjectClassTerm>
            <ccts:PropertyTerm>Acceptance</ccts:PropertyTerm>
            <ccts:BusinessProcessContextValue>EPO</ccts:BusinessProcessContextValue>
            <ccts:PrimaryRepresentationTerm>Indicator</ccts:PrimaryRepresentationTerm>
            <ccts:DictionaryEntryName>Statement. Acceptance. Indicator</ccts:DictionaryEntryName>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
      <!--ABIE property  suppressed-->
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>
