const PO_Module_Factory = function () {
  const PO = {
    name: 'PO',
    defaultElementNamespaceURI:
      'http://docs.oasis-open.org/tag/ns/v1.0/taml/201002/',
    typeInfos: [
      {
        localName: 'TextSourceItemType',
        typeName: 'textSourceItem_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            type: 'value',
          },
          {
            name: 'extension',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'extension',
            },
            type: 'attribute',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionSelectionType',
        typeName: 'testAssertionSelection_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'testAssertionSetId',
            minOccurs: 0,
            collection: true,
            typeInfo: 'NormalizedString',
          },
          {
            name: 'testAssertionIdList',
            typeInfo: '.TestAssertionSelectionType.TestAssertionIdList',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            mixed: false,
            type: 'anyElement',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'expr',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'expr',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'NormativeSourceSharedType',
        typeName: 'normativeSourceShared_type',
        baseTypeInfo: '.NormativeSourceType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'PredicateType',
        typeName: 'predicate_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'InterpretationType',
        typeName: 'interpretation_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionSetType',
        typeName: 'testAssertionSet_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'testAssertionDocumentHeader',
            typeInfo: '.TestAssertionDocumentHeaderType',
          },
          {
            name: 'shared',
            typeInfo: '.SharedType',
          },
          {
            name: 'testAssertionRef',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TestAssertionRefType',
          },
          {
            name: 'testAssertionSet',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TestAssertionSetType',
          },
          {
            name: 'testAssertionSelection',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TestAssertionSelectionType',
          },
          {
            name: 'testAssertionList',
            typeInfo: '.TestAssertionSetType.TestAssertionList',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            mixed: false,
            type: 'anyElement',
          },
          {
            name: 'id',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'id',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'schemaVersionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'schemaVersionId',
            },
            type: 'attribute',
          },
          {
            name: 'time',
            typeInfo: 'Time',
            attributeName: {
              localPart: 'time',
            },
            type: 'attribute',
          },
          {
            name: 'date',
            typeInfo: 'Date',
            attributeName: {
              localPart: 'date',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'PredicateSharedType',
        typeName: 'predicateShared_type',
        baseTypeInfo: '.PredicateType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'VarSharedType',
        typeName: 'varShared_type',
        baseTypeInfo: '.VarType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'PrerequisiteType',
        typeName: 'prerequisite_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'VarType',
        typeName: 'var_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'NamespacesType',
        typeName: 'namespaces_type',
        propertyInfos: [
          {
            name: 'content',
            collection: true,
            allowDom: false,
            elementName: 'namespace',
            typeInfo: '.NamespaceType',
            type: 'elementRef',
          },
        ],
      },
      {
        localName: 'LocationType',
        typeName: 'location_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement',
          },
        ],
      },
      {
        localName: 'TestAssertionRefType',
        typeName: 'testAssertionRef_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'testAssertionResource',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TestAssertionResourceType',
          },
          {
            name: 'testAssertionSetId',
            minOccurs: 0,
            collection: true,
            typeInfo: 'NormalizedString',
          },
          {
            name: 'testAssertionIdList',
            typeInfo: '.TestAssertionRefType.TestAssertionIdList',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            mixed: false,
            type: 'anyElement',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionSelectionType.TestAssertionIdList',
        typeName: null,
        propertyInfos: [
          {
            name: 'testAssertionId',
            minOccurs: 0,
            collection: true,
            typeInfo: 'NormalizedString',
          },
        ],
      },
      {
        localName: 'DescriptionType',
        typeName: 'description_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'RefSourceItemType',
        typeName: 'refSourceItem_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'uri',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'uri',
            },
            type: 'attribute',
          },
          {
            name: 'documentId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'documentId',
            },
            type: 'attribute',
          },
          {
            name: 'versionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'versionId',
            },
            type: 'attribute',
          },
          {
            name: 'revisionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'revisionId',
            },
            type: 'attribute',
          },
          {
            name: 'resourceProvenanceId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'resourceProvenanceId',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'AuthorsType',
        typeName: 'authors_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement',
          },
        ],
      },
      {
        localName: 'ReportSharedType',
        typeName: 'reportShared_type',
        baseTypeInfo: '.ReportType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'NamespaceType',
        typeName: 'namespace_type',
        propertyInfos: [
          {
            name: 'prefix',
            required: true,
            typeInfo: 'Token',
          },
          {
            name: 'uri',
            required: true,
          },
        ],
      },
      {
        localName: 'TagType',
        typeName: 'tag_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionType',
        typeName: 'testAssertion_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'description',
            typeInfo: '.DescriptionType',
          },
          {
            name: 'normativeSource',
            typeInfo: '.NormativeSourceType',
          },
          {
            name: 'var',
            minOccurs: 0,
            collection: true,
            typeInfo: '.VarType',
          },
          {
            name: 'target',
            typeInfo: '.TargetType',
          },
          {
            name: 'prerequisite',
            typeInfo: '.PrerequisiteType',
          },
          {
            name: 'predicate',
            typeInfo: '.PredicateType',
          },
          {
            name: 'prescription',
            typeInfo: '.PrescriptionType',
          },
          {
            name: 'report',
            minOccurs: 0,
            collection: true,
            typeInfo: '.ReportType',
          },
          {
            name: 'tag',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TagType',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            mixed: false,
            type: 'anyElement',
          },
          {
            name: 'id',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'id',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'schemaVersionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'schemaVersionId',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'NormativeSourceType',
        typeName: 'normativeSource_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'content',
            collection: true,
            allowDom: false,
            elementTypeInfos: [
              {
                elementName: 'comment',
                typeInfo: '.CommentType',
              },
              {
                elementName: 'refSourceItem',
                typeInfo: '.RefSourceItemType',
              },
              {
                elementName: 'textSourceItem',
                typeInfo: '.TextSourceItemType',
              },
              {
                elementName: 'derivedSourceItem',
                typeInfo: '.RefSourceItemType',
              },
              {
                elementName: 'interpretation',
                typeInfo: '.InterpretationType',
              },
            ],
            type: 'elementRefs',
          },
        ],
      },
      {
        localName: 'TargetSharedType',
        typeName: 'targetShared_type',
        baseTypeInfo: '.TargetType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'PrescriptionSharedType',
        typeName: 'prescriptionShared_type',
        baseTypeInfo: '.PrescriptionType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'SourceDocumentType',
        typeName: 'sourceDocument_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement',
          },
          {
            name: 'revision',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'revision',
            },
            type: 'attribute',
          },
          {
            name: 'version',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'version',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'SharedType',
        typeName: 'shared_type',
        propertyInfos: [
          {
            name: 'normativeSource',
            typeInfo: '.NormativeSourceSharedType',
          },
          {
            name: 'target',
            typeInfo: '.TargetSharedType',
          },
          {
            name: 'prerequisite',
            typeInfo: '.PrerequisiteSharedType',
          },
          {
            name: 'predicate',
            typeInfo: '.PredicateSharedType',
          },
          {
            name: 'prescription',
            typeInfo: '.PrescriptionSharedType',
          },
          {
            name: 'description',
            typeInfo: '.DescriptionSharedType',
          },
          {
            name: 'tag',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TagSharedType',
          },
          {
            name: 'var',
            minOccurs: 0,
            collection: true,
            typeInfo: '.VarSharedType',
          },
          {
            name: 'report',
            minOccurs: 0,
            collection: true,
            typeInfo: '.ReportSharedType',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            mixed: false,
            type: 'anyElement',
          },
        ],
      },
      {
        localName: 'DocumentType',
        typeName: 'document_type',
        propertyInfos: [
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'label',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'label',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'DescriptionSharedType',
        typeName: 'descriptionShared_type',
        baseTypeInfo: '.DescriptionType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionDocumentHeaderType',
        typeName: 'testAssertionDocumentHeader_type',
        propertyInfos: [
          {
            name: 'common',
            required: true,
            typeInfo: '.CommonType',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement',
          },
        ],
      },
      {
        localName: 'CommonType',
        typeName: 'common_type',
        propertyInfos: [
          {
            name: 'sourceDocument',
            typeInfo: '.SourceDocumentType',
          },
          {
            name: 'authors',
            typeInfo: '.AuthorsType',
          },
          {
            name: 'location',
            typeInfo: '.LocationType',
          },
          {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement',
          },
        ],
      },
      {
        localName: 'PrerequisiteSharedType',
        typeName: 'prerequisiteShared_type',
        baseTypeInfo: '.PrerequisiteType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionRefType.TestAssertionIdList',
        typeName: null,
        propertyInfos: [
          {
            name: 'testAssertionId',
            minOccurs: 0,
            collection: true,
            typeInfo: 'NormalizedString',
          },
        ],
      },
      {
        localName: 'PrescriptionType',
        typeName: 'prescription_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'level',
            attributeName: {
              localPart: 'level',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TargetType',
        typeName: 'target_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            typeInfo: 'NormalizedString',
            type: 'value',
          },
          {
            name: 'type',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'type',
            },
            type: 'attribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'idscheme',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'idscheme',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'ReportType',
        typeName: 'report_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement',
          },
          {
            name: 'label',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'label',
            },
            type: 'attribute',
          },
          {
            name: 'message',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'message',
            },
            type: 'attribute',
          },
          {
            name: 'when',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'when',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TagSharedType',
        typeName: 'tagShared_type',
        baseTypeInfo: '.TagType',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'conflict',
            attributeName: {
              localPart: 'conflict',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'TestAssertionSetType.TestAssertionList',
        typeName: null,
        propertyInfos: [
          {
            name: 'testAssertion',
            minOccurs: 0,
            collection: true,
            typeInfo: '.TestAssertionType',
          },
        ],
      },
      {
        localName: 'TestAssertionResourceType',
        typeName: 'testAssertionResource_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
          {
            name: 'name',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'name',
            },
            type: 'attribute',
          },
          {
            name: 'uri',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'uri',
            },
            type: 'attribute',
          },
          {
            name: 'documentId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'documentId',
            },
            type: 'attribute',
          },
          {
            name: 'versionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'versionId',
            },
            type: 'attribute',
          },
          {
            name: 'revisionId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'revisionId',
            },
            type: 'attribute',
          },
          {
            name: 'resourceProvenanceId',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'resourceProvenanceId',
            },
            type: 'attribute',
          },
        ],
      },
      {
        localName: 'CommentType',
        typeName: 'comment_type',
        propertyInfos: [
          {
            name: 'otherAttributes',
            type: 'anyAttribute',
          },
          {
            name: 'value',
            type: 'value',
          },
          {
            name: 'lg',
            typeInfo: 'NormalizedString',
            attributeName: {
              localPart: 'lg',
            },
            type: 'attribute',
          },
        ],
      },
      {
        type: 'enumInfo',
        localName: 'VarConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden'],
      },
      {
        type: 'enumInfo',
        localName: 'PredicateConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden', 'conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'PrescriptionConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden'],
      },
      {
        type: 'enumInfo',
        localName: 'NormativeSourceConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden', 'conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'ReportConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'TagConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden', 'conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'TargetConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden', 'conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'PrerequisiteConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'DescriptionConflictBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['overriding', 'overridden', 'conjunction', 'disjunction'],
      },
      {
        type: 'enumInfo',
        localName: 'PrescriptionLevelBaseCodeType',
        baseTypeInfo: 'NormalizedString',
        values: ['mandatory', 'permitted', 'preferred'],
      },
    ],
    elementInfos: [
      {
        elementName: 'testAssertionSet',
        typeInfo: '.TestAssertionSetType',
      },
      {
        elementName: 'testAssertionDocumentHeader',
        typeInfo: '.TestAssertionDocumentHeaderType',
      },
      {
        elementName: 'derivedSourceItem',
        typeInfo: '.RefSourceItemType',
        scope: '.NormativeSourceType',
      },
      {
        elementName: 'testAssertion',
        typeInfo: '.TestAssertionType',
      },
      {
        elementName: 'common',
        typeInfo: '.CommonType',
      },
      {
        elementName: 'comment',
        typeInfo: '.CommentType',
        scope: '.NormativeSourceType',
      },
      {
        elementName: 'textSourceItem',
        typeInfo: '.TextSourceItemType',
        scope: '.NormativeSourceType',
      },
      {
        elementName: 'namespace',
        typeInfo: '.NamespaceType',
        scope: '.NamespacesType',
      },
      {
        elementName: 'interpretation',
        typeInfo: '.InterpretationType',
        scope: '.NormativeSourceType',
      },
      {
        elementName: 'refSourceItem',
        typeInfo: '.RefSourceItemType',
        scope: '.NormativeSourceType',
      },
    ],
  }
  return {
    PO,
  }
}
if (typeof define === 'function' && define.amd) {
  define([], PO_Module_Factory)
} else {
  const PO_Module = PO_Module_Factory()
  if (typeof module !== 'undefined' && module.exports) {
    module.exports.PO = PO_Module.PO
  } else {
    const PO = PO_Module.PO
  }
}
