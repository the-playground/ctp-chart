const mappings = {
  name: 'TA',
  defaultElementNamespaceURI: 'http://docs.oasis-open.org/ns/tag/taml-201002/',
  typeInfos: [
    {
      type: 'classInfo',
      localName: 'TestAssertionType',
      propertyInfos: [
        {
          type: 'attribute',
          name: 'testId',
          attributeName: { localPart: 'id' },
          typeInfo: 'String',
        },
        {
          type: 'element',
          name: 'description',
          elementName: 'description',
          typeInfo: 'String',
        },
        {
          type: 'element',
          name: 'normativeSource',
          elementName: 'normativeSource',
          typeInfo: 'TA.NormativeSource',
        },
        {
          name: 'variables',
          elementName: 'var',
          minOccurs: 0,
          collection: true,
          typeInfo: 'TA.Var',
        },
        {
          type: 'element',
          name: 'target',
          elementName: 'target',
          typeInfo: 'String',
        },
        {
          type: 'element',
          name: 'prerequisite',
          elementName: 'prerequisite',
          typeInfo: 'String',
        },
        {
          type: 'element',
          name: 'predicate',
          elementName: 'predicate',
          typeInfo: 'String',
        },
        {
          type: 'element',
          name: 'prescription',
          elementName: 'prescription',
          typeInfo: 'TA.Prescription',
        },
        {
          name: 'tags',
          elementName: 'tag',
          minOccurs: 0,
          collection: true,
          typeInfo: 'TA.Tag',
        },
      ],
    },
    {
      type: 'classInfo',
      localName: 'NormativeSource',
      propertyInfos: [
        {
          type: 'element',
          name: 'normativeSource',
          elementName: 'refSourceItem',
          typeInfo: 'String',
        },
      ],
    },
    {
      type: 'classInfo',
      localName: 'Var',
      propertyInfos: [
        {
          type: 'attribute',
          name: 'attribute',
          typeInfo: 'String',
          attributeName: {
            localPart: 'vname',
          },
        },
        {
          name: 'name',
          type: 'value',
          typeInfo: 'String',
        },
      ],
    },
    {
      type: 'classInfo',
      localName: 'Prescription',
      propertyInfos: [
        {
          type: 'attribute',
          name: 'prescription',
          typeInfo: 'String',
          attributeName: {
            localPart: 'level',
          },
        },
      ],
    },
    {
      type: 'classInfo',
      localName: 'Tag',
      propertyInfos: [
        {
          type: 'attribute',
          name: 'attribute',
          typeInfo: 'String',
          attributeName: {
            localPart: 'tname',
          },
        },
        {
          name: 'name',
          type: 'value',
          typeInfo: 'String',
        },
      ],
    },
  ],
  elementInfos: [
    {
      elementName: 'testAssertion',
      typeInfo: 'TA.TestAssertionType',
    },
  ],
}

export default mappings
