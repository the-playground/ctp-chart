import { Router } from 'express'
import parametersController from '../controllers/parametersController'

const router = Router()
router.get('/', parametersController.getParameters)
router.post('/', parametersController.createParameter)
router.put('/:id', parametersController.updateParameter)
router.delete('/:id', parametersController.deleteParameter)

export default router
