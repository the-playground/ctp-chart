import connectorStatusController from '../controllers/connectorStatusController'
const { Router } = require('express')
const router = Router()

router.get('/', connectorStatusController.getConnectorStatus)

router.post('/', connectorStatusController.createConnectorStatus)

export default router
