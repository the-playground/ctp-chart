import path from 'path'
import { Router } from 'express'
const router = Router()

router.get('/:filename', (req, res) => {
  const validFiles = {
    help: 'help.pdf',
    'epo-samples': 'EPO_samples_unsigned.zip',
    'sc-samples': 'MLA_samples_unsigned.zip',
    'mla-samples': 'SC_samples_unsigned.zip',
  }

  const file = validFiles[req.params.filename]
  if (file) {
    return res.sendFile(file, { root: path.join(__dirname, '../files') })
  }
  return res.status(400).json({ error: 'Invalid file requested' })
})

export default router
