import { Router } from 'express'
import authController from '../controllers/authController'
import passport from '../passport'

const router = Router()

router.post('/auth/login', passport.authenticate('local'), authController.login)
router.get('/auth/user', passport.authenticate('jwt'), authController.fetchUser)

router.get('/auth/logout', passport.authenticate('jwt'), authController.logout)

export default router
