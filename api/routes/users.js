import { Router } from 'express'
import usersController from '../controllers/usersController'

const router = Router()
// GET ROUTES
router.get('/', usersController.getUsers)
router.get('/files/:type/:filename', usersController.getFile)
// POST ROUTES
router.post('/', usersController.createUser)
router.post('/files', usersController.uploadFile)
router.post('/filenames', usersController.submitFilenames)
// PUT ROUTES
router.put('/:id', usersController.updateUser)
// PATCH ROUTES
router.patch('/:id/password', usersController.updateUserPassword)
router.patch('/:id/roles', usersController.updateUserRoles)
// DELETE ROUTES
router.delete('/:id', usersController.deleteUser)

export default router
