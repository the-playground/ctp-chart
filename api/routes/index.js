import { Router } from 'express'
import passport from '../passport'
import messages from './messages'
import parameters from './parameters'
import services from './services'
import tests from './tests'
import files from './files'
import users from './users'
import connectorStatus from './connectorStatus'

const router = new Router()
router.use(passport.authenticate('jwt'))
router.use('/files', files)
router.use('/tests', tests)
router.use('/users', users)
router.use('/parameters', parameters)
router.use('/services', services)
router.use('/messages', messages)
router.use('/connector-status', connectorStatus)

export default router
