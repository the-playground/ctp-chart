import { Router } from 'express'
import testsController from '../../controllers/testsController'

const router = Router()

router.get('/assertions', testsController.getTestAssertions)
router.post('/assertions', testsController.createTestAssertion)
router.delete('/assertions/:id', testsController.deleteTestAssertion)
router.put('/assertions/:id', testsController.updateTestAssertion)
router.patch(
  '/assertions/:id/users/:userId',
  testsController.assignTestAssertion
)
router.delete(
  '/assertions/:id/users/:userId',
  testsController.unassignTestAssertion
)

export default router
