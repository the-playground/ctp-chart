import { Router } from 'express'
import servicesController from '../controllers/servicesController'

const router = Router()
router.get('/', servicesController.getServices)
router.post('/', servicesController.createService)
router.put('/:id', servicesController.updateService)
router.delete('/:id', servicesController.deleteService)
router.post('/:id/actions', servicesController.addAction)
router.put('/:id/actions/:actionId', servicesController.editAction)
router.delete('/:id/actions/:actionId', servicesController.deleteAction)
router.get('/:id/actions/:actionId/xml', servicesController.getActionXml)

export default router
