import { Router } from 'express'
import testsController from '../controllers/testsController'
const router = Router()

router.get('/assertions/users/:userId', testsController.getTestAssertionsByUser)
router.post(
  '/assertions/:assertionId/reports',
  testsController.createTestReport
)
router.put('/reports/:id', testsController.updateTestReport)
router.delete('/reports/:id', testsController.deleteTestReport)

export default router
