import { Router } from 'express'
import messagesController from '../controllers/messagesController'
const router = Router()

router.post('/', messagesController.createMessage)
router.post('/:id/files', messagesController.uploadMessageFile)
router.post('/:id/xml/validate', messagesController.validateXml)

router.get('/', messagesController.getMessages)
router.get('/:id/save', messagesController.submitMessage)
router.get('/:id/files', messagesController.getMessageFiles)
router.get('/updated', messagesController.getUpdatedMessages)
router.get('/predefined-files', messagesController.getPredefinedFiles)

export default router
