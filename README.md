# CTP - Nuxt
This api supports basic CTP functionalities and can be consumed both using CTP - Nuxt Web UI (available at http://ejustice.ee.auth.gr:3001/)
and direct HTTP requests.

### Disclaimer: Work in progress
This project is still being developed and cannot be considered safe to use in production.
Although, it does support authentication through Bearer Token, this is currently not enabled, as api endpoints are publicly available.

## CTP Connector API
Documentation about message creation workflow on Connector Client API that is consumed under the hood is also provided.




