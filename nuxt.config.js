import colors from 'vuetify/es5/util/colors'

export default {
  server: {
    host: '0.0.0.0',
    // port: 3001,
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - CTP',
    title: 'CTP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/style.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/vuex-orm',
    '~/plugins/vuex-orm-axios',
    '~/plugins/timeago',
    '~/plugins/snack',
    '~/plugins/api',
    '~/plugins/downloader',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'cookie-universal-nuxt',
    // https://go.nuxtjs.dev/axios
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
  serverMiddleware: ['~/api/index.js'],
  router: {
    prefetchLinks: false,
    middleware: ['auth'],
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/auth/login',
            method: 'post',
          },
          logout: {
            url: '/api/auth/logout',
            method: 'get',
          },
          user: {
            url: '/api/auth/user',
            method: 'get',
            propertyName: 'user',
          },
        },
        tokenType: 'Bearer',
        tokenRequired: true,
      },
    },
    cookie: {
      prefix: 'ctp.auth.',
    },
    scopeKey: 'roles',
    redirect: {
      login: '/login',
      callback: '/login',
      home: '/',
    },
  },
  publicRuntimeConfig: {
    partyDefaultFrom: process.env.PARTY_DEFAULT_FROM,
    partyRoles: {
      to: process.env.PARTY_ROLE_TO,
      from: process.env.PARTY_ROLE_FROM,
    },
    partyTypes: {
      to: process.env.PARTY_TYPE_TO,
      from: process.env.PARTY_TYPE_FROM,
    },
    environmentName: process.env.ENVIRONMENT_NAME,
    serverDomain: process.env.SERVER_DOMAIN,
    serverIpAddress: process.env.SERVER_IP,
    statusServerUrl: process.env.SERVER_URL,
  },
}
