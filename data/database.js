import { Database } from '@vuex-orm/core'
import User from '@/data/models/user'
import Service from '@/data/models/service'
import Parameter from '@/data/models/parameter'
import Message from '@/data/models/message'
import TestAssertion from '@/data/models/testAssertion'
import TestReport from '@/data/models/testReport'

const database = new Database()

database.register(User)
database.register(Service)
database.register(Parameter)
database.register(Message)
database.register(TestAssertion)
database.register(TestReport)

export default database
