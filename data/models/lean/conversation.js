import serviceShortcuts from '@/constants/serviceShortcuts'
export default class Conversation {
  constructor({ id, messages = [] } = {}) {
    this.id = id
    this.messages = messages
  }

  get isEmpty() {
    return this.messages.length === 0
  }

  get fromParty() {
    return this.messages[0] && this.messages[0].fromPartyId
  }

  get toParty() {
    return this.messages[0] && this.messages[0].toPartyId
  }

  get parties() {
    return this.isEmpty ? ' - ' : `${this.fromParty}, ${this.toParty}`
  }

  get lastUpdated() {
    return this.messages[0] && this.messages[0]?.created
  }

  get service() {
    if (this.isEmpty) {
      return 'None'
    }
    const service = this.messages[0].service
    const hasMultiple = this.messages.some(
      (message) => message.service !== service
    )

    return hasMultiple ? 'Mult.' : serviceShortcuts[service]
  }
}
