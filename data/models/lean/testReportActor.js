export default class TestReportActor {
  constructor({
    name = 'CTP Domibus',
    uri = '',
    contact = 'ecodex-ctp@auth.gr',
    role = 'RECEIVER',
    testCase = '',
  } = {}) {
    this.name = name
    this.uri = uri
    this.contact = contact
    this.role = role
    this.testCase = testCase
  }

  /**
   * @param {User} user
   * @returns {TestReportActor}
   */
  static createFromUser(user) {
    return new this({
      name: user.contactPerson,
      uri: user.gatewayUrl,
      contact: user.email,
      testCase: 'user',
    })
  }

  get isCustom() {
    return this.testCase === 'custom'
  }

  get isRemoved() {
    return this.testCase === 'deleted'
  }

  remove() {
    this.testCase = 'deleted'
  }

  /**
   * @param {User} user
   * @returns {boolean}
   */
  correspondsToUser(user) {
    return (
      this.contact === user.email &&
      this.name === user.contactPerson &&
      this.testCase === 'user'
    )
  }

  get isCTPActor() {
    return this.testCase === 'ctp'
  }

  toData() {
    return {
      name: this.name,
      uri: this.uri,
      contact: this.contact,
      role: this.role,
    }
  }
}
