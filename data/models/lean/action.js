export default class Action {
  constructor({
    name,
    isOutgoing,
    isReply,
    id = null,
    defaultPdfPath = null,
    defaultXmlPath = null,
    defaultXsdPath = null,
    _id = null,
  } = {}) {
    this.name = name
    this.isOutgoing = isOutgoing
    this.isReply = isReply
    this.defaultPdfPath = defaultPdfPath
    this.defaultXmlPath = defaultXmlPath
    this.defaultXsdPath = defaultXsdPath
    this.id = id || _id
  }
}
