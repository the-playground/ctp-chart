export default class MessageNotification {
  constructor({
    status = 'evidence_new',
    conversationId,
    messageId,
    evidences = [],
  } = {}) {
    this.status = status
    this.conversationId = conversationId
    this.messageId = messageId
    this.evidences = evidences
  }
}
