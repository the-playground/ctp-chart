import { Model } from '@vuex-orm/core'
import Message from '@/data/models/message'
import TestAssertion from '@/data/models/testAssertion'
import { dateOrStringToISO } from '@/utils/dateHelper'
import TestReportActor from '@/data/models/lean/testReportActor'

export default class TestReport extends Model {
  static entity = 'reports'

  static fields() {
    return {
      id: this.attr(null),
      reportId: this.string(null).nullable(),
      dateAuthored: this.attr(null),
      datePerformed: this.attr(null),
      performerName: this.string(''),
      performerEmail: this.string(''),
      actors: this.attr(() => []),
      message_id: this.attr(null),
      message: this.belongsTo(Message, 'message_id'),
      result: this.string(null).nullable(),
      issues: this.string(null).nullable(),
      warnings: this.string(null).nullable(),
      assertion_id: this.attr(null),
      assertion: this.belongsTo(TestAssertion, 'assertion_id'),
    }
  }

  static mutators() {
    return {
      actors(value) {
        return value.map((actor) => new TestReportActor(actor))
      },
    }
  }

  static deleteById(id) {
    return this.api().delete(`/api/tests/${this.entity}/${id}`, {
      delete: id,
    })
  }

  get payload() {
    return {
      performerName: this.$toJson().performerName,
      performerEmail: this.$toJson().performerEmail,
      actors: this.$toJson().actors,
      result: this.$toJson().result,
      issues: this.$toJson().issues,
      warnings: this.$toJson().warnings,
      datePerformed: dateOrStringToISO(this.datePerformed),
      dateAuthored: dateOrStringToISO(this.dateAuthored),
      message: this.message?.id,
    }
  }

  post() {
    const assertionId = this.assertion_id || this.assertion.$id
    return this.constructor
      .api()
      .post(
        `/api/tests/assertions/${assertionId}/reports`,
        { report: this.payload },
        { dataKey: 'report' }
      )
  }

  update() {
    return this.constructor
      .api()
      .put(
        `/api/tests/reports/${this.$id}`,
        { report: this.payload },
        { dataKey: 'report' }
      )
  }

  /**
   * @param {User} user
   */
  hasUserActor(user) {
    return this.actors.some((actor) => actor.correspondsToUser(user))
  }

  get hasCTPActor() {
    return this.actors.some((actor) => actor.isCTPActor)
  }

  get hasMessage() {
    return !!this.message
  }

  get isFailure() {
    return this.result === 'FAILURE'
  }
}
