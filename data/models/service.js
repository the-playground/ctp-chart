import { Model } from '@vuex-orm/core'
import Action from '@/data/models/lean/action'

export default class Service extends Model {
  static entity = 'services'

  static fields() {
    return {
      id: this.attr(null),
      name: this.string(null),
      shortcode: this.string(null),
      test: this.boolean(false),
      caseOrder: this.number(null),
      actions: this.attr([]),
    }
  }

  static mutators() {
    return {
      actions: (value) => value.map((action) => new Action(action)),
    }
  }

  static fetch() {
    if (!this.exists()) {
      return this.api().get(`/api/${this.entity}`, { dataKey: this.entity })
    }
    return Promise.resolve()
  }

  static deleteById(id) {
    return this.api().delete(`/api/${this.entity}/${id}`, {
      delete: id,
    })
  }

  post() {
    return this.constructor
      .api()
      .post('/api/services', { service: this }, { dataKey: 'service' })
  }

  update() {
    return this.constructor
      .api()
      .put(
        `/api/services/${this.$id}`,
        { service: this },
        { dataKey: 'service' }
      )
  }

  addAction(action) {
    return this.constructor
      .api()
      .post(
        `/api/services/${this.$id}/actions`,
        { action },
        { dataKey: 'service' }
      )
  }

  editAction(action) {
    if (!action.id) {
      return Promise.resolve()
    }
    return this.constructor
      .api()
      .put(
        `/api/services/${this.id}/actions/${action.id}`,
        { action },
        { dataKey: 'service' }
      )
  }

  deleteActionById(actionId) {
    return this.constructor
      .api()
      .delete(`/api/services/${this.id}/actions/${actionId}`, {
        dataKey: 'service',
      })
  }
}
