import { Model } from '@vuex-orm/core'

export default class Parameter extends Model {
  static entity = 'parameters'

  static fields() {
    return {
      id: this.attr(null),
      name: this.string(null),
      value: this.attr(null),
    }
  }

  static fetch() {
    if (!this.exists()) {
      return this.api().get(`/api/${this.entity}`, { dataKey: this.entity })
    }
    return Promise.resolve()
  }

  static deleteById(id) {
    return this.api().delete(`/api/${this.entity}/${id}`, {
      delete: id,
    })
  }

  update() {
    return this.constructor
      .api()
      .put(
        `/api/parameters/${this.$id}`,
        { parameter: this },
        { dataKey: 'parameter' }
      )
  }

  post() {
    return this.constructor
      .api()
      .post('/api/parameters', { parameter: this }, { dataKey: 'parameter' })
  }
}
