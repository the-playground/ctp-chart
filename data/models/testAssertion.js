import { Model } from '@vuex-orm/core'
import User from '@/data/models/user'
import TestReport from '@/data/models/testReport'

export default class TestAssertion extends Model {
  static entity = 'assertions'

  static fields() {
    return {
      id: this.string(null),
      testId: this.string(null).nullable(),
      normativeSource: this.string(null),
      description: this.string(null),
      prerequisite: this.string(null),
      target: this.string(null),
      prescription: this.string(null),
      predicate: this.string(null),
      tags: this.attr([]),
      variables: this.attr([]),
      user_ids: this.attr(null),
      users: this.hasManyBy(User, 'user_ids'),
      reports: this.hasMany(TestReport, 'assertion_id'),
    }
  }

  isAssignedToUser(userId) {
    return this.user_ids.includes(userId)
  }

  get group() {
    const groupTag = this.tags.find((tag) => tag.name.toUpperCase() === 'GROUP')
    return groupTag?.attribute || 'None'
  }

  get reportsCount() {
    return this.reports.length
  }

  get lastReport() {
    return this.reports?.[0]
  }

  static fetch() {
    if (!this.exists()) {
      return this.api().get(`/api/tests/${this.entity}`, {
        dataKey: this.entity,
      })
    }
    return Promise.resolve()
  }

  static fetchByUser(user) {
    if (!this.exists()) {
      return this.api().get(`/api/tests/${this.entity}/users/${user.id}`, {
        dataKey: this.entity,
      })
    }
  }

  static deleteById(id) {
    return this.api().delete(`/api/tests/${this.entity}/${id}`, {
      delete: id,
    })
  }

  post() {
    return this.constructor
      .api()
      .post(
        '/api/tests/assertions',
        { assertion: this },
        { dataKey: 'assertion' }
      )
  }

  update() {
    return this.constructor
      .api()
      .put(
        `/api/tests/assertions/${this.$id}`,
        { assertion: this },
        { dataKey: 'assertion' }
      )
  }

  assignToUser(userId) {
    return this.constructor
      .api()
      .patch(
        `/api/tests/assertions/${this.$id}/users/${userId}`,
        {},
        { dataKey: 'assertion' }
      )
  }

  unassignFromUser(userId) {
    return this.constructor
      .api()
      .delete(`/api/tests/assertions/${this.$id}/users/${userId}`, {
        dataKey: 'assertion',
      })
  }
}
