import { Model } from '@vuex-orm/core'

export default class User extends Model {
  static entity = 'users'

  static state() {
    return {
      loading: false,
    }
  }

  static fields() {
    return {
      id: this.string('').nullable(),
      username: this.string('').nullable(),
      email: this.string('').nullable(),
      gateway: this.string('').nullable(),
      finalRecipient: this.string('').nullable(),
      originalSender: this.string('').nullable(),
      contactPerson: this.string('').nullable(),
      senderId: this.string('').nullable(),
      senderAuth: this.string('').nullable(),
      receiverId: this.string('').nullable(),
      receiverAuth: this.string('').nullable(),
      roles: this.attr(null),
      gatewayVersion: this.string('').nullable(),
      connectorVersion: this.string('').nullable(),
      gatewayUrl: this.string('').nullable(),
      countryCode: this.string('').nullable(),
      xmls: this.attr([]),
      pdfs: this.attr([]),
    }
  }

  hasRole(role) {
    return (this.roles || []).includes(role)
  }

  get isPlainUser() {
    return !this.isAdmin && !this.isTester
  }

  get isAdmin() {
    return this.hasRole('ROLE_ADMIN')
  }

  get isTester() {
    return this.hasRole('ROLE_TESTER')
  }

  static fetch() {
    // Logged-in user is always present
    if (this.query().count() < 2) {
      this.commit((state) => {
        state.loading = true
      })
      return this.api()
        .get(`/api/${this.entity}`, { dataKey: this.entity })
        .finally(() =>
          this.commit((state) => {
            state.loading = false
          })
        )
    }
    return Promise.resolve()
  }

  static deleteById(id) {
    return this.api().delete(`/api/users/${id}`, {
      delete: id,
    })
  }

  static getUsers() {
    return this.query()
      .where((user) => user.isPlainUser)
      .get()
  }

  update() {
    this.constructor.commit((state) => {
      state.loading = true
    })
    const payload = {
      username: this.username,
      email: this.email,
      gateway: this.gateway,
      finalRecipient: this.finalRecipient,
      originalSender: this.originalSender,
      contactPerson: this.contactPerson,
      senderId: this.senderId,
      senderAuth: this.senderAuth,
      receiverId: this.receiverId,
      receiverAuth: this.receiverAuth,
      roles: this.roles,
      gatewayVersion: this.gatewayVersion,
      connectorVersion: this.connectorVersion,
      gatewayUrl: this.gatewayUrl,
      countryCode: this.countryCode,
    }
    return this.constructor
      .api()
      .put(`/api/users/${this.id}`, { user: payload }, { dataKey: 'user' })
      .finally(() =>
        this.constructor.commit((state) => {
          state.loading = false
        })
      )
  }

  updatePassword(passwords) {
    return this.constructor
      .api()
      .patch(`/api/users/${this.id}/password`, { passwords }, { save: false })
  }

  updateRoles(roles) {
    return this.constructor
      .api()
      .patch(`/api/users/${this.id}/roles`, { roles }, { dataKey: 'user' })
  }

  post(password) {
    return this.constructor
      .api()
      .post('/api/users', { user: this, password }, { dataKey: 'user' })
  }
}
