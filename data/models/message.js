import { Model } from '@vuex-orm/core'
import Conversation from './lean/conversation'
import fileTypes from '~/constants/messageFileTypes'
import MessageFile from '~/api/models/lean/messageFile'
import { createNotification } from '~/utils/messageNotifier'

export default class Message extends Model {
  static entity = 'messages'

  static state() {
    return {
      loading: false,
      pendingEdit: false,
      emptyConversations: [],
    }
  }

  static fields() {
    return {
      id: this.attr(null),
      ebmsMessageId: this.string(null).nullable(),
      backendMessageId: this.string(null).nullable(),
      conversationId: this.string(null).nullable(),
      originalSender: this.string(null).nullable(),
      finalRecipient: this.string(null).nullable(),
      fromPartyId: this.string(null).nullable(),
      fromPartyType: this.string(null).nullable(),
      fromPartyRole: this.string(null).nullable(),
      toPartyId: this.string(null).nullable(),
      toPartyRole: this.string(null).nullable(),
      toPartyType: this.string(null).nullable(),
      service: this.attr(null),
      action: this.attr(null),
      // ISO Date string
      created: this.string(null).nullable(),
      evidences: this.attr([]),
      files: this.attr([]),
      lastConfirmationReceived: this.string(null).nullable(),
      /**
       * @type {'PREPARED', 'SENT', 'CONFIRMED'}
       */
      messageStatus: this.string(null).nullable(),
      storageInfo: this.string(null).nullable(),
      storageStatus: this.string(null).nullable(),
      instanceId: this.string(null).nullable(),
      caseId: this.string(null).nullable(),
    }
  }

  get isDraft() {
    return this.messageStatus === 'PREPARED'
  }

  get lastEvidence() {
    return this.lastConfirmationReceived || ' - '
  }

  get properties() {
    const xmlFile = this.files.find((file) => file.fileType === fileTypes.xml)
    const pdfFile = this.files.find((file) => file.fileType === fileTypes.pdf)

    return {
      'to.party.id': this.toPartyId,
      'from.party.role': this.fromPartyRole,
      'conversation.id': this.conversationId,
      'message.received.datetime': this.created,
      'original.sender': this.originalSender,
      action: this.actionName,
      'content.xml.file.name ': xmlFile && xmlFile.fileName,
      'ebms.message.id': this.ebmsMessageId,
      'content.pdf.file.name ': pdfFile && pdfFile.fileName,
      service: this.serviceName,
      'to.party.role': this.toPartyRole,
      'from.party.id': this.fromPartyId,
      'final.recipient': this.finalRecipient,
    }
  }

  get pdfContent() {
    const pdfFile = this.files.find((file) => file.fileType === fileTypes.pdf)
    return pdfFile && pdfFile.fileContent
  }

  get xmlContent() {
    const xmlFile = this.files.find((file) => file.fileType === fileTypes.xml)
    return xmlFile && xmlFile.fileContent
  }

  get xmlName() {
    const xmlFile = this.files.find((file) => file.fileType === fileTypes.xml)
    return xmlFile && xmlFile.fileName
  }

  get pdfName() {
    const pdfFile = this.files.find((file) => file.fileType === fileTypes.pdf)
    return pdfFile && pdfFile.fileName
  }

  get actionName() {
    return typeof this.action === 'object'
      ? this.action && this.action.name
      : this.action
  }

  get serviceName() {
    return typeof this.service === 'object'
      ? this.service && this.service.name
      : this.service
  }

  get payload() {
    return {
      service: this.serviceName,
      action: this.actionName,
      conversationId: this.conversationId,
      finalRecipient: this.finalRecipient,
      originalSender: this.originalSender,
      toPartyId: this.toPartyId,
      fromPartyId: this.fromPartyId,
      toPartyRole: this.toPartyRole,
      fromPartyRole: this.fromPartyRole,
      fromPartyType: this.fromPartyType,
      toPartyType: this.toPartyType,
    }
  }

  static fetch() {
    if (!this.exists()) {
      this.commit((state) => {
        state.loading = true
      })
      return this.api()
        .get('/api/messages', { dataKey: 'messages' })
        .finally(() =>
          this.commit((state) => {
            state.loading = false
          })
        )
    }
    return Promise.resolve()
  }

  /**
   * @return {MessageNotification[]}
   */
  static fetchUpdated() {
    this.commit((state) => (state.loading = true))

    return this.api()
      .get('/api/messages/updated', { dataKey: 'messages' })
      .then((result) =>
        (result.entities.messages || []).map((message) =>
          createNotification(
            message,
            this.query().whereId(message.id).exists(),
            this.query()
              .where('conversationId', message.conversationId)
              .exists()
          )
        )
      )
      .finally(() => this.commit((state) => (state.loading = false)))
  }

  /**
   * @return {Conversation[]}
   */
  static getConversations() {
    /** @type Message[] */
    const messages = this.query().orderBy('id', 'desc').get()
    const conversations = []
    messages.forEach((message) => {
      const cid = message.conversationId
      const conversation = conversations.find((con) => con.id === cid)
      if (conversation) {
        conversation.messages.push(message)
        return
      }
      conversations.push(
        new Conversation({
          id: cid,
          messages: [message],
        })
      )
    })

    this.store().state.entities.messages.emptyConversations.forEach(
      (conversation) => {
        if (!conversations.some((con) => con.id === conversation.id)) {
          conversations.unshift(conversation)
        }
      }
    )

    return conversations
  }

  /**
   * Post message, store it to vuex orm db and return id
   * @return {string} new message id
   */
  post() {
    this.constructor.commit((state) => {
      state.pendingEdit = true
    })
    return this.constructor
      .api()
      .post('/api/messages', { message: this.payload }, { dataKey: 'message' })
      .then((result) => result.entities.messages[0].id)
      .finally(() =>
        this.constructor.commit((state) => {
          state.pendingEdit = false
        })
      )
  }

  /**
   * @param {string} fileName
   * @param {string} fileContent base64 encoded XML (without base64 prefix)
   * @param {"xml"|"pdf"|"other"} type
   * @return {Promise<void>}
   */
  uploadFile(fileName, fileContent, type) {
    this.constructor.commit((state) => {
      state.pendingEdit = true
    })
    const fileType = fileTypes[type]
    const payload = { fileName, fileContent, fileType }
    return this.constructor
      .api()
      .post(`/api/messages/${this.$id}/files`, payload, { save: false })
      .then(() => {
        this.files.push(payload)
      })
      .finally(() =>
        this.constructor.commit((state) => {
          state.pendingEdit = false
        })
      )
  }

  save() {
    this.constructor.commit((state) => {
      state.pendingEdit = true
    })
    return this.constructor
      .api()
      .get(`/api/messages/${this.$id}/save`, { save: false })
      .finally(() =>
        this.constructor.commit((state) => {
          state.pendingEdit = false
        })
      )
  }

  /**
   * @return {Promise<void>}
   */
  loadFileContents() {
    this.constructor.commit((state) => {
      state.loading = true
    })
    return this.constructor
      .api()
      .get(`/api/messages/${this.$id}/files`, { save: false })
      .then((result) => {
        const { data } = result.response

        const messageFiles = this.files.map(
          (file) =>
            new MessageFile({
              ...file,
              fileContent: data.files[file.fileName] || null,
            })
        )
        return this.constructor.update({
          where: (record) => record.id === this.id,
          data: { ...this.$toJson(), files: messageFiles },
        })
      })
      .finally(() =>
        this.constructor.commit((state) => {
          state.loading = false
        })
      )
  }

  toJSON() {
    return Object.getOwnPropertyNames(this).reduce((a, b) => {
      a[b] = this[b]
      return a
    }, {})
  }
}
