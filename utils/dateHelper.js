const dateOrStringToISO = (dateOrString) => {
  if (!dateOrString) {
    return null
  }
  const dateObject =
    dateOrString instanceof Date ? dateOrString : new Date(dateOrString)
  return dateObject.toISOString()
}

export { dateOrStringToISO }
