import MessageNotification from '@/data/models/lean/messageNotification'

/**
 * @param {Object} newMessage
 * @param {Boolean} messageExists
 * @param {Boolean} conversationExists
 * @return {MessageNotification}
 */
const createNotification = (newMessage, messageExists, conversationExists) => {
  const status = !conversationExists
    ? 'conversation_new'
    : !messageExists
    ? 'message_new'
    : 'evidence_new'
  return new MessageNotification({
    status,
    conversationId: newMessage.conversationId,
    messageId: newMessage.id,
    evidences: newMessage.evidences,
  })
}

export { createNotification }
