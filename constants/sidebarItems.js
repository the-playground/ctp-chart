export default [
  {
    title: 'Dashboard',
    icon: 'mdi-view-dashboard',
    route: '/',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'Messages',
    icon: 'mdi-email-outline',
    route: '/messages',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'Manage tests',
    icon: 'mdi-bug-check-outline',
    route: '/tests/manage',
    roles: ['ROLE_ADMIN'],
  },
  {
    title: 'My Tests',
    icon: 'mdi-bug-check-outline',
    route: '/tests',
    roles: ['ROLE_USER'],
  },
  {
    title: 'Log Viewer',
    icon: 'mdi-clipboard-alert-outline',
    route: 'logs',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'Material',
    icon: 'mdi-file-document-multiple-outline',
    route: '/material',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'Help',
    icon: 'mdi-help',
    route: '/help',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'About',
    icon: 'mdi-information-outline',
    route: '/about',
    roles: ['ROLE_USER', 'ROLE_ADMIN'],
  },
  {
    title: 'User Monitor',
    icon: 'mdi-clipboard-account-outline',
    route: 'user-monitor',
    roles: ['ROLE_ADMIN'],
  },
  {
    title: 'Manage users',
    icon: 'mdi-account-multiple-outline',
    route: '/users/manage',
    roles: ['ROLE_ADMIN'],
  },
  {
    title: 'Manage services',
    icon: 'mdi-cube-scan',
    route: '/services/manage',
    roles: ['ROLE_ADMIN'],
  },
  {
    title: 'Manage parameters',
    icon: 'mdi-cogs',
    route: '/parameters/manage',
    roles: ['ROLE_ADMIN'],
  },
  {
    title: 'e-CODEX PLUS',
    icon: 'mdi-account-multiple-plus-outline',
    route: 'e-codex-plus',
    roles: ['ROLE_E_CODEX_PLUS', 'ROLE_ADMIN'],
  },
]
