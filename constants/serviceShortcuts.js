export default {
  'BC-009MLA': 'MLA',
  'Business Registers': 'BR',
  'Connector-TEST': 'Con-T',
  EPO: 'EPO',
  'Financial Penalties': 'FP',
  'GW-TEST': 'GW-T',
  'Mutual Legal Assistance': 'MLA',
  SmallClaims: 'SC',
}
