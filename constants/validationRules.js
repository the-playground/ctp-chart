export const isEmpty = (value) =>
  value === null ||
  value === undefined ||
  (typeof value === 'string' && !value.trim()) ||
  (Array.isArray(value) && !value.length)

const validateEmail = (value) => {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return pattern.test(value)
}

const validatePasswordFormat = (value) => {
  const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/
  return pattern.test(value)
}

const rules = {
  required: (value) => !isEmpty(value) || 'This field is required',
  email: (value) => isEmpty(value) || validateEmail(value) || 'Invalid e-mail',
  requiredEmail: (value) => !isEmpty(value) || 'Email is required',
  requiredPassword: (value) => !isEmpty(value) || 'Password is required',
  validPasswordLength: (value) =>
    (!isEmpty(value) && value.length > 5 && value.length < 21) ||
    'Password must contain between 6 and 20 characters',
  validPassword: (value) =>
    validatePasswordFormat(value) ||
    'Password should contain at least one lowercase letter, one uppercase letter and one number',
  isNumber: (value) => !value || !isNaN(value) || 'This field is numeric',
  isInteger: (value) =>
    !value ||
    (!isNaN(value) && Number.isInteger(parseFloat(value))) ||
    'This field is an integer',
}

export default rules
