export const state = () => ({
  xmlBase64Strings: [],
  pdfBase64Strings: [],
})

export const mutations = {
  /**
   *
   * @param {Object} state
   * @param {String[]} base64Strings
   */
  setXmlBase64Strings: (state, base64Strings) => {
    state.xmlBase64Strings = base64Strings
  },
  /**
   *
   * @param {Object} state
   * @param {String[]} base64Strings
   */
  setPdfBase64Strings: (state, base64Strings) => {
    state.pdfBase64Strings = base64Strings
  },
}

export const actions = {
  async fetchPredefinedFiles({ state, commit }) {
    if (state.xmlBase64Strings.length && state.pdfBase64Strings.length) {
      return
    }
    /**
     * @type {{xmls: string[], pdfs: []}}
     */
    const base64Strings = await this.$api.messages.getPredefinedFiles()

    commit('setXmlBase64Strings', base64Strings.xmls)
    commit('setPdfBase64Strings', base64Strings.pdfs)
  },
}
