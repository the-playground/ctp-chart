export const state = () => ({
  hasFeedbackSnack: false,
  feedbackType: 'success',
  feedbackContent: '',
})

export const mutations = {
  /**
   * @param state
   * @param value
   */
  setHasFeedbackSnack: (state, value) => {
    state.hasFeedbackSnack = value
  },
  /**
   * @param state
   * @param feedback
   */
  setFeedbackContent: (state, feedback) => {
    state.feedbackContent = feedback
  },
  /**
   * @param state
   * @param type
   */
  setFeedbackType: (state, type) => {
    state.feedbackType = type
  },
}

export const actions = {
  toggleFeedbackSnack: ({ commit }, { message, type = 'success' }) => {
    commit('setFeedbackContent', message)
    commit('setFeedbackType', type)
    commit('setHasFeedbackSnack', true)
  },
}
